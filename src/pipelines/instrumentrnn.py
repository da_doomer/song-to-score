from .pipelines import ReportingPipeline
from functools import reduce
import numpy as np
from torch import nn
from torch import optim
import torch
from tqdm import tqdm
import gc
from random import shuffle

import time

from src.reporting import PDFManager

from torchsummary import summary
import matplotlib.pyplot as plt

from parameters import quick

class RNN(nn.Module):
    """Expects numpy data.
    Constructor specification:
        x_shape: (feature11, feature12,...)
        y_shape: (feature21, feature22,...)

    Forward specification:
        x.shape: (batch, seq_len, feature11, feature12,...)
        y.shape: (batch, feature21, feature22,...)

    Elman layer:
        http://www2.fiit.stuba.sk/~kvasnicka/NeuralNetworks/6.prednaska/Elman_RNN.pdf
    """
    def __init__(self, x_shape, y_shape):
        """
        x_shape: (batch, seq_len, notes)
        y_shape: (batch, seq_len, channels)
        """
        super(RNN,self).__init__()
        self.x_shape = x_shape
        self.y_shape = y_shape
        self.total_input = reduce(lambda i,j: i*j, x_shape)
        self.total_output = reduce(lambda i,j: i*j, y_shape)

        self.notes = x_shape[-1]
        self.out_channels = y_shape[-1]

        # Dense
        self.dense_layers = 0
        self.dense_size = 512

        # LSTM
        self.recurrent_layers = 2
        self.bidirectional = True
        self.rnn_dropout = 0.3
        self.hidden_size = 512

        # Add dense layers
        self.initial_layers = list()
        self.initial_layers.append( nn.Linear(
                in_features = self.notes,
                out_features = self.dense_size,
            )
        )
        self.add_module("densei{}".format(0),self.initial_layers[-1])
        for n in range(self.dense_layers):
            self.initial_layers.append(
                nn.Linear(
                    in_features = self.dense_size,
                    out_features = self.dense_size,
                )
            )
            self.add_module("dense{}".format(n),self.initial_layers[-1])
            self.initial_layers.append(
                    torch.nn.LeakyReLU()
            )
            self.add_module("sigmoid{}".format(n),self.initial_layers[-1])


        # Add recurrent layer
        self.rnn = nn.LSTM(
                input_size = self.dense_size,
                hidden_size = self.hidden_size,
                num_layers = self.recurrent_layers,
                batch_first = True,
                dropout = self.rnn_dropout,
                bidirectional = self.bidirectional,
        )

        # Add final layers
        directions = 2 if self.bidirectional == True else 1
        self.final_layers = list()
        self.final_layers.append(
                nn.Linear(
                    in_features = self.hidden_size*directions,
                    out_features = self.notes,
                )
            )
        self.add_module("densefin{}".format(2),self.final_layers[-1])
        #self.final_layers.append(
        #        nn.LeakyReLU()
        #    )
        #self.add_module("activation{}".format(1),self.final_layers[-1])
        self.final_layers.append(
                nn.Linear(
                    in_features = self.notes,
                    out_features = self.out_channels,
                )
            )
        self.add_module("densefin{}".format(3),self.final_layers[-1])

    def forward(self, x):
        # x.shape: (batch, seq_len, notes)
        # Gather information
        batch_size = x.shape[0]
        notes_size = x.shape[-1]
        seq_len = x.shape[1]
        recurrent_layers = self.recurrent_layers
        directions = 2 if self.bidirectional == True else 1
        hidden_size = self.hidden_size

        # Pass through dense layers to (batch, seq_len, dense_size)
        y = torch.sign(x)
        for layer in self.initial_layers:
            y = layer(y)

        # Recurrently predict each element in the sequence
        # Hidden state is defaulted to zero
        h0 = torch.randn(recurrent_layers*directions, batch_size, hidden_size)
        c0 = torch.randn(recurrent_layers*directions, batch_size, hidden_size)
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        h0, c0 = h0.to(device), c0.to(device)
        y, hidden = self.rnn(y, (h0, c0))

        # Pass through final layers
        for layer in self.final_layers:
            y = layer(y)

        # Element-wise multiplication
        y = torch.mul(x,y)

        # y shape without batch dimension should be y_shape
        if y.shape[1:] != self.y_shape[1:]:
            raise Exception("In {} final y.shape was {} but {}"
                            " was requested".format(
                                str(type(self).__name__),
                                y.shape[1:],
                                self.y_shape))
        return y

class InstrumentRNNPipeline(ReportingPipeline):
    def run(self):
        pdf_filename = "{}report.pdf".format(str(type(self).__name__))
        report_pdf = PDFManager(pdf_filename)
        self.details.append(pdf_filename)

        # TODO add batching and batch shuffling
        # Parameters
        settings = {
                'epochs':512,
                'seq_len':8,
                #'optim_params': {'lr':0.01, 'momentum':0.9},
                'optim_params': {},
                'batch_size':32,
        }
        if quick: settings['epochs'] = 4

        # Add settings to report
        self._report.append("\n".join([str((x,y)) for x,y in settings.items()]))

        # Sequence data
        seq_len = settings['seq_len']
        def seqxy(x,y,nobin=True, index=True):
            # Transform (samples,  88, channels) to indexed (samples, 88)
            if index:
                newy = np.full((y.shape[0],y.shape[1]),0)
                for sample_i in range(len(y)):
                    for note_i in range(len(y[sample_i])):
                        if set(y[sample_i][note_i].flatten()) == set([0]):
                            # Skip frames where no instrument is playing
                            continue
                        channel = np.argmax(y[sample_i][note_i], axis=-1)
                        newy[sample_i][note_i] = channel + 1
                y = newy

            bx,by = list(), list()
            #for i in range(0, len(x)-seq_len,seq_len):
            for i in range(0, len(x)-seq_len, round(seq_len**1/2)):
                bx.append(x[i:i+seq_len])
                by.append(y[i:i+seq_len])
            bx, by = np.array(bx), np.array(by)
            # Binarize output (<=0.0 = -1; else = 1)
            if nobin: return bx, by
            for arr in [bx]:
                for ind,val in np.ndenumerate(arr):
                    if val <= 0: arr[ind] = 0
                    else: arr[ind] = 1
            return bx,by
        x_train = list()
        y_train = list()
        x_val = list()
        y_val = list()
        x_test = list()
        y_test = list()
        for x,y in self.dataset.train:
            bx,by = seqxy(x,y)
            x_train.extend(bx)
            y_train.extend(by)
        x_train = np.array(x_train)
        y_train = np.array(y_train)
        for x,y in self.dataset.val:
            bx,by = seqxy(x,y)
            x_val.extend(bx)
            y_val.extend(by)
        x_val = np.array(x_val)
        y_val = np.array(y_val)
        for x,y in self.dataset.test:
            bx,by = seqxy(x,y, index=False)
            x_test.extend(bx)
            y_test.extend(by)
        x_test = np.array(x_test)
        y_test = np.array(y_test)

        self._report.append("Sequenced data to sequences of length {}".format(seq_len))
        self._report.append("Target data is the instrument index of each note".format(seq_len))
        self._report.append("Recurrently orchestrate x1...x{n} to y1...y{n}".format(n=seq_len))
        self._report.append("Recurrently orchestrate x1...x{n} to y1...y{n}".format(n=seq_len))
        self._report.append("Pipline x shape: {}".format(x_train.shape))
        self._report.append("Pipline y shape: {}".format(y_train.shape))

        # Prepare CUDA
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        if torch.cuda.is_available():
            print("CUDA is available for pytorch")
            print("Using device {}".format(device))

        # Build RNN using dataset shape
        rnn = RNN(x_train.shape, y_train.shape).to(device)
        optimizer = optim.RMSprop(rnn.parameters(), **settings['optim_params'])
        loss_func = nn.SmoothL1Loss()

        # Write RNN summary to report
        stringlist = []
        self._report.append("Architeture details:\n{}".format(str(rnn)))
        n_trainable_params = sum(p.numel() for p in rnn.parameters() if p.requires_grad)
        self._report.append("Trainable paremeters:\n{}".format(str(n_trainable_params)))
        self._report.append("Optimizer details:\n{}".format(str(optimizer)))
        self._report.append("Loss details:\n{}".format(str(loss_func)))
        self._report.append("In prediction, only notes"
                " that existed in the input are assigned an instrument")

        def batch(l, batch_size = 5):
            for i in range(0,len(l)-batch_size,batch_size):
                b = l[i:i+batch_size]
                x, y = [bi[0] for bi in b],[bi[1] for bi in b]
                yield np.array(x), np.array(y)

        # Prepare training timer
        training_time = time.time()

        # Train RNN
        average_losses = list()
        for epoch in tqdm(range(settings['epochs']),desc="Training"):
            epoch_losses = list()
            avgloss, curr = 0,1
            xy = list(zip(x_train, y_train))
            shuffle(xy)
            xy = list(batch(xy,batch_size=settings['batch_size']))
            with tqdm(
                    xy,
                    total=len(xy),
                    desc="Epoch {}".format(str(epoch))
                    ) as tasks:
                for inp, target in tasks:
                    # Build target and input tensors
                    inp = np.array(inp, dtype = 'float32')
                    inp = torch.from_numpy(inp)
                    target = np.array(target, dtype='float32')
                    target = torch.from_numpy(target)

                    # Send tensors to GPU (or CPU if CUDA unavailable)
                    inp = inp.to(device)
                    target = target.to(device)

                    # Step optimizer
                    optimizer.zero_grad()
                    output = rnn(inp)
                    loss = loss_func(output, target)
                    loss.backward()
                    optimizer.step()

                    # Update bar
                    avgloss = (avgloss*(curr-1) + loss.item())/curr
                    curr += 1
                    tasks.set_postfix(average_loss=avgloss)

                    # Update losses
                    #epoch_losses.append(loss)
            # Update average losses
            average_losses.append(avgloss)

            gc.collect()

        # Report training time
        training_time = time.time() - training_time
        self._report.append("Training took {}s on {}".format(round(training_time),device))

        f, ax = plt.subplots()
        ax.set_title("Loss vs epoch")
        ax.plot(range(len(average_losses)),average_losses)
        report_pdf.add_fig(f)

        self._report.append("Average loss value for each training epoch: {}".format(str(average_losses)))

        # Predict on testing data and binarize
        y_pred = list()
        for inp in tqdm(x_test, desc="NaiveRNN predicting testing data"):
            if set(inp.flatten()) == set([0]):
                # Skip empty frames
                yp = np.zeros(y_test.shape[1:])
            else:
                x = inp
                inp = np.array([inp], dtype = 'float32')
                inp = torch.from_numpy(inp).to(device)
                yp = rnn(inp)
                yp = yp.cpu().detach().numpy()[0]
                yp2 = np.zeros(y_test.shape[1:])
                # x.shape : (batch, 88)
                # yp.shape : (batch, 88)
                # yp2.shape : (batch, 88, channels)
                for sample_i in range(len(yp)):
                    for note_i in range(len(yp[sample_i])):
                        # Skip frames where no instrument was playing
                        #if set(x[sample_i][note_i].flatten()) == set([0]):
                        #    continue
                        channel = int(round(yp[sample_i][note_i]))-1
                        # Skip frames where no instrument is predicted playing
                        if channel < 0:
                            continue
                        channel = min(yp2.shape[2]-1, channel)
                        yp2[sample_i][note_i][channel] = 1
                yp = yp2
            y_pred.append(yp)

        # Calculate MSE on binarized data
        mse_errors = list()
        for ypr, ytr in zip(y_pred, y_test):
            mse_errors.append(((ypr.flatten()-ytr.flatten())**2).mean())
        mse = sum(mse_errors)/len(mse_errors)
        self._report.append("Average MSE on predicted testing data: {}".format(mse))
        return x_test, y_pred, y_test
