from .pipelines import ReportingPipeline
from functools import reduce
from itertools import starmap
import numpy as np
from torch import nn
from torch import optim
import torch
from tqdm import tqdm
import gc
from random import shuffle

import time

from src.reporting import PDFManager

from torchsummary import summary
import matplotlib.pyplot as plt

from parameters import quick

from concurrent.futures import ThreadPoolExecutor

from multiprocessing import Pool

# Pytorch does not support padding = SAME
# Source:
# https://github.com/pytorch/pytorch/issues/3867#issuecomment-407663012
class Conv1dSame(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size,
            padding_layer=torch.nn.ReflectionPad1d,**kwargs):
        super().__init__()
        ka = kernel_size // 2
        kb = ka - 1 if kernel_size % 2 == 0 else ka
        self.net = torch.nn.Sequential(
            padding_layer((ka,kb)),
            torch.nn.Conv1d(in_channels, out_channels, kernel_size, **kwargs)
        )
    def forward(self, x):
        return self.net(x)

class RNN(nn.Module):
    """Expects numpy data.
    Constructor specification:
        x_shape: (feature11, feature12,...)
        y_shape: (feature21, feature22,...)

    Forward specification:
        x.shape: (batch, seq_len, feature11, feature12,...)
        y.shape: (batch, feature21, feature22,...)

    Elman layer:
        http://www2.fiit.stuba.sk/~kvasnicka/NeuralNetworks/6.prednaska/Elman_RNN.pdf
    """
    def __init__(self, x_shape, y_shape):
        """
        x_shape: (batch, seq_len, notes)
        y_shape: (batch, seq_len, notes, channels)
        """
        super(RNN,self).__init__()
        self.x_shape = x_shape
        self.y_shape = y_shape
        self.total_input = reduce(lambda i,j: i*j, x_shape)
        self.total_output = reduce(lambda i,j: i*j, y_shape)

        self.notes = x_shape[-1]
        self.instruments = y_shape[-1]
        self.out_channels = self.notes * self.instruments

        # Dense
        self.dense_layers = 0
        self.dense_size = 512 # Each instrument will be reduced to 3 octaves

        # LSTM
        self.recurrent_layers = 2
        self.bidirectional = True
        self.rnn_dropout = 0.3
        self.hidden_size = 5096//2

        # Add dense layers
        self.initial_layers = list()
        self.initial_layers.append( nn.Linear(
                in_features = self.notes,
                out_features = self.dense_size,
            )
        )
        self.add_module("densei{}".format(0),self.initial_layers[-1])
        for n in range(self.dense_layers):
            self.initial_layers.append(
                nn.Linear(
                    in_features = self.dense_size,
                    out_features = self.dense_size,
                )
            )
            self.add_module("dense{}".format(n),self.initial_layers[-1])
            self.initial_layers.append(
                    torch.nn.LeakyReLU()
            )
            self.add_module("activation{}".format(n),self.initial_layers[-1])


        # Add recurrent layer
        self.rnn = nn.LSTM(
                input_size = self.dense_size,
                hidden_size = self.hidden_size,
                num_layers = self.recurrent_layers,
                batch_first = True,
                dropout = self.rnn_dropout,
                bidirectional = self.bidirectional,
        )

        # Add final layers
        directions = 2 if self.bidirectional == True else 1
        self.final_layers = list()
        self.final_layers.append(
                nn.Linear(
                    in_features = self.hidden_size*directions,
                    out_features = self.out_channels,
                )
            )
        self.add_module("densefin{}".format(2),self.final_layers[-1])
        #self.final_layers.append(
        #        nn.Sigmoid()
        #    )
        #self.add_module("activation{}".format(1),self.final_layers[-1])
        #self.final_layers.append(
        #        nn.Linear(
        #            in_features = self.out_channels,
        #            out_features = self.out_channels,
        #        )
        #    )
        #self.add_module("densefin{}".format(3),self.final_layers[-1])
        self.final_layers.append(
                nn.Sigmoid()
            )
        self.add_module("activation{}".format(1),self.final_layers[-1])

    def forward(self, x):
        # x.shape: (batch, seq_len, notes)
        # Gather information
        batch_size = x.shape[0]
        notes_size = x.shape[-1]
        seq_len = x.shape[1]
        recurrent_layers = self.recurrent_layers
        directions = 2 if self.bidirectional == True else 1
        hidden_size = self.hidden_size

        # Pass through dense layers to (batch, seq_len, dense_size)
        y = torch.sign(x)
        for layer in self.initial_layers:
            y = layer(y)

        # Recurrently predict each element in the sequence
        # Hidden state is defaulted to zero
        h0 = torch.randn(recurrent_layers*directions, batch_size, hidden_size)
        c0 = torch.randn(recurrent_layers*directions, batch_size, hidden_size)
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        h0, c0 = h0.to(device), c0.to(device)
        y, hidden = self.rnn(y, (h0, c0))

        # Pass through final layers
        for layer in self.final_layers:
            y = layer(y)

        # Element-wise multiplication
        y = torch.reshape(y, (batch_size, seq_len, self.notes, self.instruments))
        mask = torch.stack([torch.sign(x)]*self.instruments,dim=-1)
        y = torch.mul(y, mask)

        # y shape without batch dimension should be y_shape
        if y.shape[1:] != self.y_shape[1:]:
            raise Exception("In {} final y.shape was {} but {}"
                            " was requested".format(
                                str(type(self).__name__),
                                y.shape[1:],
                                self.y_shape[1:]))
        return y

def seqxy(xy,seq_len):
    x, y = xy
    bx,by = list(), list()
    for i in range(0, len(x)-seq_len,seq_len):
        bx.append(x[i:i+seq_len])
        by.append(y[i:i+seq_len])
    bx, by = np.array(bx), np.array(by)
    # Binarize output (<=0.0 = -1; else = 1)
    #for arr in [by]:
    #    for ind,val in np.ndenumerate(arr):
    #        if val <= 0: arr[ind] = 0
    #        else: arr[ind] = 1
    return bx,by

def seqdataset(dst,seq_len):
    seqx = list()
    seqy = list()
    params = [(xy, seq_len) for xy in dst]
    with Pool() as p:
        results = list(p.starmap(seqxy, params))
    for bx,by in results:
        seqx.extend(bx)
        seqy.extend(by)
    return np.array(seqx), np.array(seqy)

class NaiveRNNPipeline(ReportingPipeline):
    def run(self):
        pdf_filename = "{}report.pdf".format(str(type(self).__name__))
        report_pdf = PDFManager(pdf_filename)
        self.details.append(pdf_filename)

        # TODO add batching and batch shuffling
        # Parameters
        settings = {
                'epochs':1024,
                'seq_len':16,
                #'optim_params': {'lr':0.01, 'momentum':0.9},
                'optim_params': {},
                'batch_size':32,
        }
        if quick: settings['epochs'] = 4

        # Add settings to report
        self._report.append("\n".join([str((x,y)) for x,y in settings.items()]))

        # Add dataset stats to report
        self._report.append("Found {}/{}/{} songs for train/val/test".format(
                    str(len(list(self.dataset.train))),
                    str(len(list(self.dataset.val))),
                    str(len(list(self.dataset.test))),
             ))

        # Sequence data
        seq_len = settings['seq_len']

        # Time preprocessing
        preprocessing_time = time.time()
        results = list(tqdm(starmap(seqdataset,
                [
                    (list(self.dataset.train), seq_len),
                    (list(self.dataset.val), seq_len),
                    (list(self.dataset.test), seq_len),
                ]
                ),desc="Transforming train/val/test datasets",total=3))
        preprocessing_time = time.time() - preprocessing_time
        self._report.append("Preprocessing took {}s".format(round(preprocessing_time)))

        x_train, y_train = results[0]
        x_val, y_val = results[1]
        x_test, y_test = results[2]

        self._report.append("Sequenced data to sequences of length {}".format(seq_len))
        self._report.append("Target data is the (note,channel) probability of activation".format(seq_len))
        self._report.append("Recurrently orchestrate x1...x{n} to y1...y{n}".format(n=seq_len))
        self._report.append("Recurrently orchestrate x1...x{n} to y1...y{n}".format(n=seq_len))
        self._report.append("Pipline x shape: {}".format(x_train.shape))
        self._report.append("Pipline y shape: {}".format(y_train.shape))

        # Prepare CUDA
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        if torch.cuda.is_available():
            print("CUDA is available for pytorch")
            print("Using device {}".format(device))

        # Build RNN using dataset shape
        rnn = RNN(x_train.shape, y_train.shape).to(device)
        optimizer = optim.ASGD(rnn.parameters(), **settings['optim_params'])
        #loss_func = nn.BCEWithLogitsLoss()
        loss_func = nn.MSELoss(reduction="sum")

        # Write RNN summary to report
        stringlist = []
        self._report.append("Architeture details:\n{}".format(str(rnn)))
        n_trainable_params = sum(p.numel() for p in rnn.parameters() if p.requires_grad)
        self._report.append("Trainable paremeters:\n{}".format(str(n_trainable_params)))
        self._report.append("Optimizer details:\n{}".format(str(optimizer)))
        self._report.append("Loss details:\n{}".format(str(loss_func)))
        self._report.append("In prediction, only notes"
                " that existed in the input are assigned an instrument")

        def batch(l, batch_size = 5):
            for i in range(0,len(l)-batch_size,batch_size):
                b = l[i:i+batch_size]
                x, y = [bi[0] for bi in b],[bi[1] for bi in b]
                yield np.array(x), np.array(y)

        # Prepare training timer
        training_time = time.time()

        # Train RNN
        average_losses = list()
        xy = list(zip(x_train, y_train))
        for epoch in tqdm(range(settings['epochs']),desc="Training"):
            epoch_losses = list()
            avgloss, curr = 0,1
            shuffle(xy)
            batched_xy = list(batch(xy,batch_size=settings['batch_size']))
            with tqdm(
                    batched_xy,
                    desc="Epoch {}".format(str(epoch))
                    ) as tasks:
                for i,(inp, target) in enumerate(tasks):

                    # Build target and input tensors
                    inp = np.array(inp, dtype = 'float32')
                    inp = torch.from_numpy(inp)
                    inp = inp.to(device)

                    target = np.array(target, dtype='float32')
                    target = torch.from_numpy(target)
                    target = target.to(device)

                    optimizer.zero_grad()

                    # Process input on network in GPU
                    output = rnn(inp)

                    # Step optimizer
                    loss = loss_func(output, target)
                    loss.backward()
                    optimizer.step()

                    # Update bar
                    avgloss = (avgloss*(curr-1) + loss.item())/curr
                    curr += 1
                    tasks.set_postfix(average_loss=avgloss)

                    # Update losses
                    #epoch_losses.append(loss)
            # Update average losses
            average_losses.append(avgloss)

            #gc.collect()

        # Report training time
        training_time = time.time() - training_time
        self._report.append("Training took {}s on {}".format(round(training_time),device))

        f, ax = plt.subplots()
        ax.set_title("Loss vs epoch")
        ax.plot(range(len(average_losses)),average_losses)
        report_pdf.add_fig(f)

        self._report.append("Average loss value for each training epoch: {}".format(str(average_losses)))

        # Predict on testing data and binarize
        y_pred = list()
        for inp in tqdm(x_test, desc="NaiveRNN predicting testing data"):
            # Binarize output (<=0.0 = 0; else = 1)
            x = inp
            inp = np.array([inp], dtype = 'float32')
            inp = torch.from_numpy(inp).to(device)
            yp = rnn(inp)
            yp = yp.cpu().detach().numpy()[0]
            y_pred.append(yp)

        for yp in y_pred:
            for ind,val in np.ndenumerate(yp):
                if val <= 0.5: yp[ind] = 0
                else: yp[ind] = 1

        # Calculate MSE on binarized data
        mse_errors = list()
        for ypr, ytr in zip(y_pred, y_test):
            mse_errors.append(((ypr.flatten()-ytr.flatten())**2).mean())
        mse = sum(mse_errors)/len(mse_errors)
        self._report.append("Average MSE on binarized predicted testing data: {}".format(mse))
        return x_test, y_pred, y_test
