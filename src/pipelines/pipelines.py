"""
This file defines the ReportingPipeline ABC: a reporting design pattern (mostly)
unrelated to sklearn pipelines.
"""

from abc import ABC
from abc import abstractmethod
from typing import List

class ReportingPipeline(ABC):
    """
    ReportingPipelines are documented ML algorithms that receive a Dataset and
    output predictions on the testing data.

    They have two important properties:
        - The *report* property is a list of strings that describe the steps
          needed to replicate the reported results. The list should be a
          human-readable summary detailed enough to re-implement the python
          program.
        - The *details* property is a list of PDF filenames that provide
          additional information. They complement the *report* property with
          figures (e.g. tensorflow architecture or convolutional filter plots),
          tables (e.g. sklearn regression model hyperparameters), etc.
     """
    def __init__(self,dataset):
        """
        ReportingPipelines MUST NOT override this constructor.
        """
        self.dataset = dataset
        self._report = list()
        self._details = list()

    @abstractmethod
    def run(self):
        """Preprocess, train, and predict on the Dataset (the usual ML
        pipeline).

        At each step the *self._report* list must be expanded with (a string of
        the) details of the performed operations. E.g. if performing PCA the
        list would be expanded with:
            Performed PCA on the data using sklearn.PCA with the following
            parameters:
                n_components = 5
                whiten = False
                ... etc

        This method MUST NOT access Dataset.test *y* data.

        Additional details can be indicated by building a PDF file and adding
        the filename to the *self._details* list.

        Returns
        -------
        x_test, y_pred, y_true
            tuple of lists: Dataset.test predicted *x* data and target (true)
            data.
        """
        return

    @property
    def report(self) -> List[str]:
        return self._report
    @property
    def details(self) -> List[str]:
        return self._details
