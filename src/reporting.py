"""This file defines operations to build a PDF report file."""
# External imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from sklearn.metrics import classification_report

# Python imports
from typing import List, Dict
from threading import RLock
from os import remove, rename
from os.path import isfile

from pdfrw import PdfReader, PdfWriter

class PDFManager:
    """Thread safe representation of a PDF file"""
    def __init__(self, pdf_dest : str):
        self.pdf_dest = str(pdf_dest)
        self.lock = RLock()

    def add_pdf(self, pdf : str, bookmark : str = None):
        """Appends pdf to pdf_dest. If the file
        did not exist it is created."""
        with self.lock:
            filename3 = "./temp3.pdf"
            writer = PdfWriter()
            if isfile(self.pdf_dest):
                writer.addpages(PdfReader(self.pdf_dest).pages)
            writer.addpages(PdfReader(pdf).pages)
            writer.write(filename3)
            rename(filename3, self.pdf_dest)

    def add_fig(self, f : Figure, show : bool = False):
        """Convinience function. Adds the plot f to the pdf"""
        with self.lock:
            fig_pdf = "./fig.pdf"
            f.savefig(fname=fig_pdf,format='pdf')
            f.clear()
            plt.close(f)
            self.add_pdf(fig_pdf)
            remove(fig_pdf)

    def add_strings(self, strings):
        """Write a list of strings to filename in PDF format"""
        with self.lock:
            filename3 = "./strings.pdf"
            from fpdf import FPDF
            pdf = FPDF()
            pdf.add_page()
            pdf.set_font('Arial', '', 12)
            for string in strings:
                pdf.multi_cell(0, 5, string)
            pdf.output(filename3, 'F')
            self.add_pdf(filename3)
            remove(filename3)

import matplotlib

def plot_pianoroll(grid, ax):
    """Build a piano roll using grid[len][88][instruments]"""
    grid = np.array(grid).transpose(1,0,2)
    matrix = np.empty(grid.shape[:2])
    for ind, _ in np.ndenumerate(matrix):
        color = -1
        for inst,val in enumerate(grid[ind]):
            if val > 0:
                color = inst
        matrix[ind] = color   
    # Mask 0 values to show special color
    #matrix = np.ma.array(matrix, mask=(matrix == np.nan))
    cmap = matplotlib.cm.get_cmap('Set1')
    cmap.set_under(color='white')
    ax.imshow(matrix,cmap=cmap,origin='lower',vmin=0,vmax=grid.shape[-1])
    ax.set_aspect('auto')
    ax.set_yticks([0,88])
