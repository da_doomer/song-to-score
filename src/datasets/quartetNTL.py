from .datasets import Dataset
from pathlib import Path
from ..midiutils import midi_to_ntl_lists
from tqdm import tqdm
import traceback
from memory_profiler import profile

class QuartetNTLDataset(Dataset):
    @property
    def x_shape(self): return (88,)
    @property
    def y_shape(self): return (88,4,)
    @profile
    def build(self):
        midi_files = list(Path("./data/midiquartets/").glob("*mid"))
        errors = 0
        for midi in tqdm(midi_files, desc="Opening dataset {}".format(type(self).__name__)):
            try:
                # TODO: add window_size, stride and OH encoding as
                # parameters
                X, Y = midi_to_ntl_lists(midi, 30, 5,
                                         onehot_indexes=True)

                # TODO: update this safety code, but might not be needed
                # anymore
                
                # for xi, yi in zip(x,y):
                #     if xi.shape != self.x_shape:
                #         raise Exception("x shape should be {} but is {}"
                #                         " on file {}".format(
                #                             self.x_shape,
                #                             xi.shape,
                #                             midi))
                #     if yi.shape != self.y_shape:
                #         raise Exception("y shape should be {} but is {}"
                #                         " on file {}".format(
                #                             self.y_shape,
                #                             yi.shape,
                #                             midi))
            except Exception as e:
                print("ERROR reading {}".format(midi))
                print(traceback.print_exc())
                errors += 1
                continue
            
            for x, y in zip(X,Y):
                yield x, y
                
        if errors > 0:
            print("Exception occurred on {}/{} files on {}".format(
                str(errors),
                len(midi_files),
                str(type(self).__name__)))
