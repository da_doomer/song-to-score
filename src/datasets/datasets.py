"""
This file downloads and prepares datasets.
"""
from abc import ABC
from abc import abstractmethod
#from mido import MidiFile
from random import choice
from random import shuffle
import tempfile

from parameters import quick

class Dataset(ABC):
    """
    Read-only entities with the following properties:
        - name  : short, human readable string representation of the dataset
        - desc  : verbose human readable description of the dataset
        - train : sized iterable of sequences of (x,y) pairs
        - val   : sized iterable of sequences of (x,y) pairs
        - test  : sized iterable of sequences of (x,y) pairs
        - x_shape : x shape integer tuple.
        - y_shape : y shape integer tuple.

    Each sequence of (x,y) pairs represents a song. *x* are notes (88), *y* are
    orchestations (88,instruments_n).

    Iterable properties (train, val, test) can be iterated an arbitrary number
    of times (unlike raw generators).

    On creation, Datasets are shuffled.

    Batching and caching is part of specific data pipelines.
    Audio segmentation and preprocessing is part of specific data pipelines.

    To add a data source you must inherit from this ABC and implement the
    build() method.

    https://mido.readthedocs.io/en/latest/midi_files.html
    """
    @property
    @abstractmethod
    def x_shape(self) -> tuple: pass
    @property
    @abstractmethod
    def y_shape(self) -> tuple: pass
    @abstractmethod
    def build(self):
        """
        This method yields (x,y) pairs constrained to the specification of the
        Dataset class.
        On call, you can download from the internet, perform necessary
        conversions and build the corresponding MIDI files.
        """
        while False:
            yield None

    def __init__(self,train_frac : float, val_frac : float, test_frac : float):
        """
        Build a dataset using the given distribution settings.

        Parameters
        ----------
        train_frac : float
            Fraction of the data to use for training (approximate)
        val_frac : float
            Fraction of the data to use for training (approximate).
        test_frac : float
            Fraction of the data to use for training (approximate).
        """
        TRAIN, VAL, TEST = 0,1,2
        self._train, self._val, self._test = list(), list(), list()
        destinations = { TRAIN : self._train, VAL : self._val, TEST : self._test}
        # Try to add at least one element to each of train, val and test
        safety = 0
        for i,(x,y) in enumerate(self.build()):
            if quick:
                print("Cropping dataset to 10% because parameters.quick = True")
                newlen = len(x)//10
                x = x[:newlen]
                y = y[:newlen]
            if safety == 0:
                d = TRAIN
            elif safety == 1:
                d = VAL
            elif safety == 2:
                d = TEST
            #elif quick:
            #    print("Stopping dataset opening because parameters.quick = True")
            #    break
            else: # Shuffle destination.
                fracs = [train_frac, val_frac, test_frac]
                den = min([f for f in fracs if f > 0.0])
                tr, vl, te = map(lambda frac: int(frac//den), fracs)
                d = choice([TRAIN]*tr + [VAL]*vl + [TEST]*te)
            safety += 1
            destinations[d].append((x, y))

        # Shuffle list order
        for destination in destinations.values():
            shuffle(destination)

    def _generate(self, xy):
        for x,y in xy: yield x, y
    @property
    def train(self):
        return self._generate(self._train)
    @property
    def val(self):
        return self._generate(self._val)
    @property
    def test(self):
        return self._generate(self._test)
