from .datasets import Dataset
from pathlib import Path
from ..midiutils import data_from_midi_padded
from tqdm import tqdm
import traceback
from memory_profiler import profile

from multiprocessing import Pool

def data_from_midi_padded_wrapper(*args):
    try:
        result = data_from_midi_padded(*args)
        return result
    except Exception as e:
        print("ERROR reading {}".format(str(args)))
        print(traceback.print_exc())
        return -1

class DownloadDataset(Dataset):
    @property
    def x_shape(self): return (88,)
    @property
    def y_shape(self): return (88,129,)
    @profile
    def build(self):
        midi_files = list(Path("./data/composingai_downloadmidi/").glob("*mid"))
        errors = 0
        with Pool() as p:
            for midi in tqdm(
                    p.imap(data_from_midi_padded_wrapper,midi_files),
                    total = len(midi_files),
                    desc="Opening dataset {}".format(type(self).__name__),
                    ):
                # If error opening dataset, skip
                if midi == -1:
                    errors += 1
                    continue
                x, y = midi
                for xi, yi in zip(x,y):
                    if xi.shape != self.x_shape:
                        raise Exception("x shape should be {} but is {}"
                                        " on file {}".format(
                                            self.x_shape,
                                            xi.shape,
                                            midi))
                    if yi.shape != self.y_shape:
                        raise Exception("y shape should be {} but is {}"
                                        " on file {}".format(
                                            self.y_shape,
                                            yi.shape,
                                            midi))
                yield x, y
        if errors > 0:
            print("Exception occurred on {}/{} files on {}".format(
                str(errors),
                len(midi_files),
                str(type(self).__name__)))
