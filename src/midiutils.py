import mido, numpy

# Transform list of notes to timeline form
def notes_to_timeline(a, gran):
    time = 0
    on_notes = []
    tl = []

    for msg in a:
        while msg[0] > time:
            tl.append(on_notes.copy())
            time = time + gran

        if msg[1] == 1:
            on_notes.append(msg[2])
        else:
            on_notes.remove(msg[2])

    return tl

# Transforms one frame of a timeline to one-hot encoding
def one_hot_frame(a):
    oh = [0] * 88
    for note in a:
        #in case of repeated notes
        oh[note] = oh[note] + 1
    return oh

# Transforms a timeline to one-hot encoding
def one_hot_timeline(tl):
    return [one_hot_frame(frame) for frame in tl]

# Generates X,Y data for track separation task
def data_from_midi(path):
    try:
        midi = mido.MidiFile(path)
    except:
        raise IOError("File doesn't exist")

    tracks_notes = []

    # Assume the first track is metadata
    for track in midi.tracks[1:]:
        notes = []
        # Start time count
        time = 0

        # Filter only on/off messages
        for msg in filter(lambda m : m.type in ["note_on", "note_off"], track):
            time = time + msg.time

            # Messages are (time, 0/1, piano number)
            # We change midi number to piano number, 0-indexed
            notes.append((time, 1 if msg.type == "note_on" else 0, msg.note - 21))

        tracks_notes.append(notes)

    # Append all tracks and sort by time
    all_notes = [msg for track in tracks_notes for msg in track]
    all_notes.sort()

    # Ticks per beat (pulses per quarter note)
    tpb = midi.ticks_per_beat

    # This granularity should work for up to 16th triplets (tpb / 24)
    # and is a power of 2
    # Really this should always be a divisor of tpb or something
    # according to the time signature

    #TODO: add as parameter or find a smarter way to set
    gran = 128

    # Transforming data to timelines
    all_tl = notes_to_timeline(all_notes, gran)
    tracks_tl = [notes_to_timeline(track, gran) for track in tracks_notes]

    # Padding shorter tracks
    for tr in tracks_tl:
        while len(tr) < len(all_tl):
            tr.append([])

    # Transforming timelines to one-hot encoding
    all_oh = one_hot_timeline(all_tl)
    tracks_oh = [one_hot_timeline(track) for track in tracks_tl]

    # TODO check output shape,
    # x should be n,88
    # y should be n,88,4
    # hotfix transpose
    return (numpy.array(all_oh,dtype='short'), numpy.array(tracks_oh,dtype='short').transpose(1,2,0))

class InvalidTrackException(Exception):
    pass

def get_track_channel(track):
    """
    Returns an index between [0,...,15]

    https://en.wikipedia.org/wiki/General_MIDI#Program_change_events
    """
    # Get first non meta message
    for msg in track:
        if hasattr(msg,"channel"):
            return msg.channel
    raise InvalidTrackException("Track was all meta messages.")

def get_track_instrument_id(track):
    """
    Returns an index between [0,...,128]

    https://en.wikipedia.org/wiki/General_MIDI#Program_change_events
    """
    # Get channel. If channel == 9 (10 minus 1), then drums.
    track_channel = get_track_channel(track)
    if track_channel == 9: return 128

    # If not drums, then get first program change and assume it remains static.
    for msg in track:
        if not msg.is_meta and msg.type == "program_change":
            return msg.program
    raise InvalidTrackException("Track had no program_change messages.")

# Generates X,Y data for track separation task
def data_from_midi_padded(path):
    """
    Generates X,Y data for track separation task padded to a 128 instrument
    array, instead of using few channels.
    """
    try:
        midi = mido.MidiFile(path)
    except:
        raise IOError("File doesn't exist")

    # Always define 129 tracks (128 General MIDI instruments + Drums)
    tracks_notes = [[]]*129

    # Assume the first track is metadata
    for track in midi.tracks:
        # Obtain instrument ID
        try:
            instrument_id = get_track_instrument_id(track)
        except InvalidTrackException as e:
            #print(e)
            #print("Ignoring invalid track, continue to parse")
            continue

        notes = []
        # Start time count
        time = 0

        # Filter only on/off messages
        for msg in filter(lambda m : m.type in ["note_on", "note_off"], track):
            time = time + msg.time

            # Messages are (time, 0/1, piano number)
            # We change midi number to piano number, 0-indexed
            notes.append((time, 1 if msg.type == "note_on" else 0, msg.note - 21))

        tracks_notes[instrument_id] = notes

    # Append all tracks and sort by time
    all_notes = [msg for track in tracks_notes for msg in track]
    all_notes.sort()

    # Ticks per beat (pulses per quarter note)
    tpb = midi.ticks_per_beat

    # This granularity should work for up to 16th triplets (tpb / 24)
    # and is a power of 2
    # Really this should always be a divisor of tpb or something
    # according to the time signature

    #TODO: add as parameter or find a smarter way to set
    gran = 128

    # Transforming data to timelines
    all_tl = notes_to_timeline(all_notes, gran)
    tracks_tl = [notes_to_timeline(track, gran) for track in tracks_notes]

    # Padding shorter tracks
    for tr in tracks_tl:
        while len(tr) < len(all_tl):
            tr.append([])

    # Transforming timelines to one-hot encoding
    all_oh = one_hot_timeline(all_tl)
    tracks_oh = [one_hot_timeline(track) for track in tracks_tl]

    # TODO check output shape,
    # x should be n,88
    # y should be n,88,4
    # hotfix transpose
    return (numpy.array(all_oh,dtype='short'), numpy.array(tracks_oh,dtype='short').transpose(1,2,0))


# Returns a list of pairs (X,Y) where each X is a list of triples
# (Note, Time, Length) representing the notes played by all midi tracks
# and Y is a list of the same length with the index of the track of
# the corresponding note

# window_size is the length of each X or Y. If None is provided, the
# return list contains only one pair (X, Y), corresponding to all the
# notes in the midi

# stride is the number of notes between the start of consecutive windows.
# If None is provided, then stride is default to window_size, that is,
# the windows are just non-overlapping.

# track defines which notes to mark in Y lists. If a valid track index is
# provided, values in Y lists corresponding to that track are 1 and others
# are 0. Otherwise, Y lists contain the index of the track corresponding to
# note.
def midi_to_ntl_lists(path, window_size=None, stride=None,
                      focus_track=None, onehot_indexes=False):
    try:
        midi = mido.MidiFile(path)
    except:
        raise IOError("Error opening MIDI file at " +
                      path)

    # NTL triples per track
    ntl_per_track = []

    for track in midi.tracks:
        ntl_list = []
        time = 0
        on_notes = dict()

        for msg in filter(lambda m : m.type in ["note_on", "note_off"],
                          track):
            time = time + msg.time

            if(msg.type == "note_on"):
                if msg.note in on_notes:
                    on_notes[msg.note].append(time)
                else:
                    on_notes[msg.note] = [time]
            else:
                if msg.note in on_notes:
                    # This might nos handle simultaneous same-note
                    # apparitions correctly
                    start_time = on_notes[msg.note][0]
                    on_notes[msg.note] = on_notes[msg.note][1:]
                    if(on_notes[msg.note] == []):
                        on_notes.pop(msg.note)
                        
                    ntl_list.append([msg.note,
                                     start_time,
                                     time - start_time])
                else:
                    raise IOError("Corrupted MIDI file?")

        ntl_per_track.append(ntl_list)

    # If first track was metadata, remove it
    if ntl_per_track[0] == []:
        ntl_per_track = ntl_per_track[1:]

    if focus_track != None and focus_track >= len(ntl_per_track):
        raise IOError("focus_track needs to be a valid track index.")

    # merge NTL from all tracks
    all_ntls = []
    for i, ntl_list in enumerate(ntl_per_track):
        for ntl in ntl_list:
            all_ntls.append((ntl, i))

    # sort by start time
    all_ntls.sort(key=lambda p: p[0][1])

    # transform to focus track version if needed
    if focus_track != None:
        focused_ntls = []
        for ntl, track_index in all_ntls:
            if focus_track == track_index:
                focused_ntls.append((ntl, 1))
            else:
                focused_ntls.append((ntl, 0))
        all_ntls = focused_ntls

    # transform track indexes to one hot encoding if needed
    # HOTFIX: fixing number of tracks (and encoding size) to 4
    # TODO: dinamically adjust encoding size? or find better arch
    if onehot_indexes:
        if focus_track != None:
            raise ValueError("focus_track not compatible with " +
                             "onehot_indexes")
        oh_encoded = []
        for ntl, track_index in all_ntls:
            oh = [0] * 4
            oh[track_index] = 1
            oh_encoded.append((ntl, oh))
        all_ntls = oh_encoded
        
    # transform to windowed version if needed
    all_ntl_windows = []
    if window_size != None:
        if window_size > len(all_ntls):
            raise IndexError("Window size is too big")

        if stride == None:
            stride = window_size

        # This can exclude some elements at the end of the list
        for i in range(0, len(all_ntls), stride):
            if(i + window_size < len(all_ntls)):
                all_ntl_windows.append(all_ntls[i:i + window_size])
    else:
        all_ntl_windows = [all_ntls]
        
    # unzipping lists of pairs into a pairs of lists
    ntl_ls = []
    idx_ls = []
    # unzipped_ntl_windows = []
    for window in all_ntl_windows:
        ntls, indexes = zip(*window)
        ntls = list(ntls)
        indexes = list(indexes)
        ntl_ls.append(ntls)
        idx_ls.append(indexes)

    # return all_ntl_windows
    return (numpy.array(ntl_ls), numpy.array(idx_ls))


# Returns the MIDI encoded as a list of events appropriate
# for the Transformer generation task
# Shape of return value is (e, 512) where e is the number if events
# resulting from the encoding.
#
# Each event is encoded as a 512-sized one hot vector corresponding as
# following:
#     0 - 127     note on
#   128 - 255     note off
#   256 - 383     short time deltas (1 - 512 ticks)
#   384 - 447     medium time deltas (513 - 1536 ticks)
#   448 - 511     long time deltas (1537 - )
def midi_to_event_list(path):

    try:
        midi = mido.MidiFile(path)
    except:
        raise IOError("Error opening MIDI file at " +
                      path)

    all_msgs = []
    # for i, track in enumerate(midi.tracks):
    for track in midi.tracks:
        time = 0

        for msg in filter(lambda m : m.type in ["note_on", "note_off"],
                          track):
            time += msg.time
            
            if(msg.type == "note_on"):
                all_msgs.append((time, 1, msg.note))
            # This will not check for turning off notes that are not
            # currently active
            # TODO: maybe check this
            else:
                all_msgs.append((time, 0, msg.note))

    # sort by time then type then note
    all_msgs.sort()

    # Add time delta events
    events = []
    prevtime = 0
    for time, etype, note in all_msgs:
        # if time has passed add delta
        if prevtime < time:
            events.append((2, time - prevtime))
        events.append((etype, note))
        prevtime = time

    # encode events
    # vocab size is 128 on events + 128 off events + 128 short deltas +
    # 64 medium deltas + 64 long deltas = 512
    #
    # TODO: find a better way to distribute deltas
    def encode_event(event):
        etype, value = event
        enc = [0] * 512
        # 128 on events
        if etype == 1:
            i = value
        # 128 off events
        elif etype == 0:
            i = 128 + value
        else:
            # 128 short delta events (1-512)
            if value <= 512:
                i = 256 + int(numpy.round(value/4))
            else:
                value -= 512
                # 64 medium delta events (513 - 1536)
                if value <= 1024:
                    i = 384 + int(numpy.round(value/16))
                # 64 long delta events (1537 - )
                else:
                    value -= 1024
                    # if delta is too big (~3 bars) it gets trimmed
                    value = min(value, 2047)
                    i = 448 + int(numpy.round(value/64))
                    i = min(511, i)
        enc[i] = 1;
        return enc

    encoded_events = []
    for event in events:
        encoded_events.append(encode_event(event))
    
    return numpy.array(encoded_events)
