"""
When run as main this file will:
    1. Download and prepare the dataset.
    2. Build and train an ANN
    3. Write to the specified file training and testing data.
"""
from parameters import report_filename
from parameters import active_tasks
from parameters import train_frac, val_frac, test_frac

from src.reporting import PDFManager
from src.reporting import plot_pianoroll

import traceback

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

import os
import random

def main():
    errors = list()
    report_pdf = PDFManager(report_filename)
    for Dataset, Pipeline in active_tasks:
        try:
            dataset = Dataset(train_frac, val_frac, test_frac)
            pipeline = Pipeline(dataset)
            x_test, y_pred, y_true = pipeline.run()
            print("Pipeline {} report:".format(str(type(pipeline).__name__)))
            print("\n".join(pipeline.report))

            # Add pipeline.report strings to report PDF
            report_pdf.add_strings(pipeline.report)

            # Add pipeline.details PDFs to report PDF
            for pdf in pipeline.details:
                report_pdf.add_pdf(pdf)
                os.remove(pdf)

            # Add y_pred/y_true piano roll comparison
            xypyt = random.choices(list(zip(x_test, y_pred, y_true)),k=10)
            for x, yp, yt in tqdm(xypyt,desc="Plotting piano roll comparison"):
                f, axs = plt.subplots(ncols=3)
                axs[0].set_title("Input data")
                axs[1].set_title("Predicted data")
                axs[2].set_title("Target data")
                plot_pianoroll(x[...,np.newaxis], axs[0])
                plot_pianoroll(yp, axs[1])
                plot_pianoroll(yt, axs[2])
                f.tight_layout()
                report_pdf.add_fig(f)

        except Exception as e:
            error = "ERROR on pipeline {} and dataset {}".format(
                str(Pipeline.__name__),
                str(Dataset.__name__))
            trace = traceback.print_exc()
            errors.append(error)
            errors.append(trace)

    print("All pipelines finished")
    if len(errors) == 0: print("No pipeline errors reported")
    else: print("\n".format(errors))
    print("Report PDF saved to {}".format(report_filename))

if __name__ == "__main__":
    main()
