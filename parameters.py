"""
This file documents execution settings and hyperparameters.
"""
from pathlib import Path
from typing import Final
import matplotlib.pyplot as plt

quick:  Final =                  True               # If QUICK analysis
quick:  Final =                  False               # If QUICK analysis

from src.datasets.quartet import QuartetDataset
from src.datasets.composingaidownloadmidi import DownloadDataset

from src.pipelines.naivernn import NaiveRNNPipeline
from src.pipelines.indexrnn import IndexRNNPipeline
from src.pipelines.instrumentrnn import InstrumentRNNPipeline

# General settings
report_filename: Final =         Path("report.pdf")  # report filename
if quick: report_filename =      Path("quick.pdf")  # report filename
writable_dir =                   Path("tmp")         # "Persistent" storage

# Plotting settings
cmap : Final =                   plt.cm.Blues        # Matplotlib color scheme

# Dataset settings
train_frac, val_frac, test_frac = 0.4,0.0,0.6
active_tasks = [
    (QuartetDataset, NaiveRNNPipeline),
    #(QuartetDataset, IndexRNNPipeline),
    #(QuartetDataset, InstrumentRNNPipeline),
    (DownloadDataset, NaiveRNNPipeline),
        ]
